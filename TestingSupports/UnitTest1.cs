using System;
using TestProj.Utility;
using Xunit;

namespace TestingSupports
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            //i expect 9 to be return
            decimal[] tempArray = new decimal[] { 1, 3, 6, 12, 9, 15, 12 };
            var result = Utility.GetMedian(tempArray);
            Assert.Equal(9, result);
        }
    }
}
