﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace TestProj.Utility
{
    public static class Utility
    {
        public static List<decimal> ConvertCSVtoList(string strFilePath)
        {
            int UseIndex = 0;
            List<string[]> values = new List<string[]>();
            List<decimal> ExcelValues = new List<decimal>();
            using (StreamReader reader = new StreamReader(strFilePath))
            {
                while (!reader.EndOfStream)
                {
                    var dataToAdd = reader.ReadLine().Split(',');
                    if (UseIndex > 0)
                    {
                        ExcelValues.Add(Convert.ToDecimal(dataToAdd[UseIndex]));
                    }
                    values.Add(dataToAdd);
                    if (values[0].Contains("Energy"))
                    {
                        UseIndex = 7;
                    }
                    else if (values[0].Contains("Data Value"))
                    {
                        UseIndex = 5;
                    }
                    else
                    {
                        UseIndex = 0;
                    }
                }
            }
            return ExcelValues;
        }


        public static decimal GetMedian(decimal[] tempArray)
        {
            int count = tempArray.Length;

            Array.Sort(tempArray);
            Console.WriteLine(tempArray);

            decimal medianValue = 0;

            if (count % 2 == 0)
            {
                // count is even, need to get the middle two elements, add them together, then divide by 2
                decimal middleElement1 = tempArray[(count / 2) - 1];
                decimal middleElement2 = tempArray[(count / 2)];
                medianValue = (middleElement1 + middleElement2) / 2;
            }
            else
            {
                // count is odd, simply get the middle element.
                medianValue = tempArray[(count / 2)];
            }

            return medianValue;
        }


    }
}
