﻿(function () {
    'use strict';

    angular.module('myApp', [])
        .service('PersonService', ['$http', '$q', function ($http, $q) {

            var fac = {};
            var config = {
                headers: { 'Content-Type': 'application/json' }
            }

            fac.GetPersonList = function () {
                return $http.get('/Person/GetList').then(function (response) {
                    return response.data;
                })
            }

            fac.GetPersonByID = function (PersonID) {

                var deferred = $q.defer();
                $http.get('/Person/GetByID', { params: { PersonID: PersonID } }, config).success(function (response) {
                    deferred.resolve(response);
                }).error(function (err) {
                    console.log(err);
                    deferred.reject(err);
                });
                return deferred.promise;
            }

            fac.CreatePerson = function (model) {
                var data = {
                    FirstName: model.firstName,
                    LastName: model.lastName,
                    Email: model.email,
                    Address: model.address,
                    DepartmentName: model.departmentName
                };
                var deferred = $q.defer();
                $http.post('/Person/Create', data, config).success(function (response) {
                    deferred.resolve(response);
                }).error(function (err) {
                    console.log(err);
                    deferred.reject(err);
                });
                return deferred.promise;
            }

            fac.UpdatePerson = function (model) {
                var data = {
                    FirstName: model.firstName,
                    LastName: model.lastName,
                    Email: model.email,
                    Address: model.address,
                    DepartmentName: model.departmentName,
                    PersonID: model.personID
                };
                var deferred = $q.defer();
                $http.post('/Person/Update', data, config).success(function (response) {
                    deferred.resolve(response);
                }).error(function (err) {
                    console.log(err);
                    deferred.reject(err);
                });
                return deferred.promise;
            }

            fac.DeletePerson = function (PersonID) {
                var deferred = $q.defer();
                $http.get('/Person/Delete', { params: { PersonID: PersonID } }, config).success(function (response) {
                    deferred.resolve(response);
                }).error(function (err) {
                    console.log(err);
                    deferred.reject(err);
                });
                return deferred.promise;
            }

            return fac;

        }])
        .controller('PersonCtrl', ['$scope', 'PersonService', function ($scope, PersonService) {
            var vm = this;
            vm.PersonDetails = {};
            function reloadGrid() {
                PersonService.GetPersonList().then(function (data) {
                    vm.PersonList = data;

                });
            }
            reloadGrid();
            vm.OpenNew = function () {
                vm.PersonDetails = {};
                vm.IsEdit = false;
            }
            vm.OpenEditModal = function (PersonID) {
                vm.IsEdit = true;
                PersonService.GetPersonByID(PersonID).then(function (data) {
                    vm.PersonDetails = data;
                });
            }
            vm.CreatePerson = function (model) {
                PersonService.CreatePerson(model).then(function (data) {
                    $.notify("Successully created!", "success");
                    reloadGrid();
                });
            }
            vm.UpdatePerson = function (model) {
                PersonService.UpdatePerson(model).then(function (data) {
                    $.notify("Successully updated!", "success");
                    reloadGrid();
                });
            }
            vm.DeletePerson = function (PersonID) {
                if (confirm("Are you sure you want to delete?")) {
                    PersonService.DeletePerson(PersonID).then(function (data) {
                        // debugger
                        $.notify("Successully deleted!", "success");
                        reloadGrid();
                    });
                }
                else {
                    return false;
                }

            }
        }])
        .controller('ExcelCtrl', ['$scope', function ($scope) {
            var vm = this;
            vm.ImportDataExcel = function () {
                var fileExtension = ['xls', 'xlsx','csv'];
                var filename = $('#fUpload').val();
                if (filename.length == 0) {
                    alert("Please select a file.");
                    return false;
                }
                else {
                    var extension = filename.replace(/^.*\./, '');
                    if ($.inArray(extension, fileExtension) == -1) {
                        alert("Please select only excel files.");
                        return false;
                    }
                }
                var fdata = new FormData();
                debugger
                var fileUpload = $("#fUpload").get(0);
                var files = fileUpload.files;
                fdata.append(files[0].name, files[0]);

                $.ajax({
                    url: '/Export/ExportExcel',
                    processData: false,
                    contentType: false,
                    data: data,
                    type: 'POST'
                }).done(function (result) {
                    alert(result);
                }).fail(function (a, b, c) {
                    console.log(a, b, c);
                })
            }
        }]);
})();
