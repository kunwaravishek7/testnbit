﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TestProj.Database.Tables
{
    public class Person
    {
        [Key]
        public int PersonID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string DepartmentName { get; set; }
        public string Email { get; set; }
        public DateTime? CreatedTS { get; set; }
        public DateTime? ModifiedTS { get; set; }
    }
}
