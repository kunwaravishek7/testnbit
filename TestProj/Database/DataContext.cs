﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestProj.Database.Tables;

namespace TestProj.Database
{
    public class DataContext:IdentityDbContext
    {
        public DataContext(DbContextOptions options):base(options)
        {

        }

        public DbSet<Person> Persons { get; set; }
    }
}
