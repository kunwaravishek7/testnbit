﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Web;
using System.Net.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Text;
using TestProj.ViewModels;

namespace TestProj.Controllers
{
    [Produces("application/json")]
    [Route("ImportExcel")]
    public class ExcelController : Controller
    {
        private IHostingEnvironment _env;
        public ExcelController(IHostingEnvironment env)
        {
            _env = env;
        }
        [ActionName("Importexcel")]
        [HttpPost]
        public ActionResult Importexcel()
        {
            MinMedianMaxViewModel result = new MinMedianMaxViewModel();
            IFormFile file = Request.Form.Files[0];
            string webRootPath = _env.WebRootPath;
            string folderName = "Upload";
            string newPath = Path.Combine(webRootPath, folderName);
            string fullPath = Path.Combine(newPath, file.FileName);
            StringBuilder sb = new StringBuilder();
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
                using (FileStream fs = System.IO.File.Create(fullPath))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                }
            }
            else
            {
                if (!System.IO.File.Exists(fullPath))
                {
                    using (FileStream fs = System.IO.File.Create(fullPath))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }
                }
            }
            if (Request.Form.Files["FileUpload1"].Length > 0)
            {
                string extension  = Path.GetExtension(file.FileName).ToLower();
                string[] validFileTypes = { ".xls", ".xlsx", ".csv" }; //Avishek: for now only csv
               
             
                if (validFileTypes.Contains(extension))
                {
                    if (extension == ".csv")
                    {
                        List<decimal> ExcelValues = Utility.Utility.ConvertCSVtoList(fullPath);
                        result.MinValue=ExcelValues.Min();
                        result.MaxValue = ExcelValues.Max();
                        decimal[] ExcelValuesToArray = ExcelValues.ToArray();
                        result.MedianValue = Utility.Utility.GetMedian(ExcelValuesToArray);
                       
                    }

                }
                else
                {
                    ViewBag.Error = "Please Upload Files in .csv format";

                }
                System.IO.File.Delete(fullPath); //remove file from Directory

            }
            return View(result);

        }



    }
}