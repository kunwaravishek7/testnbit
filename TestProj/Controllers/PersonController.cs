﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestProj.Database.Tables;
using TestProj.Services.Base;

namespace TestProj.Controllers
{
    [Produces("application/json")]
    [Route("api/Person")]
    public class PersonController : Controller
    {
       // protected IPersonRepository _personRepository;
        private IRepositoryBase<Person> _repository = null;
        public PersonController(IRepositoryBase<Person> repository)
        {
            _repository = repository;
        }

        [HttpGet]
        [Route("/Person/GetList")]
        public IActionResult GetAllPerson()
        {
            var result=  _repository.GetAll();
            return Ok(result);
        }

        [HttpGet]
        [Route("/Person/GetByID")]
        public IActionResult GetByID(int PersonID)
        {
            var result = _repository.GetById(PersonID);
            return Ok(result);
        }

        [HttpPost]
        [Route("/Person/Create")]
        public IActionResult CreatePerson([FromBody] Person model)
        {
            model.CreatedTS = DateTime.Now;
            _repository.Insert(model);
            _repository.Save();
            return Ok();
        }

        [HttpPost]
        [Route("/Person/Update")]
        public IActionResult UpdatePerson([FromBody] Person model)
        {
            model.ModifiedTS = DateTime.Now;
           _repository.Update(model);
            _repository.Save();
            return Ok();
        }

        [Route("/Person/Delete")]
        public ActionResult Delete(int PersonID)
        {
            _repository.Delete(PersonID);
            _repository.Save();
            return Ok();
        }
        }
    }