﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestProj.ViewModels
{
    public class MinMedianMaxViewModel
    {
        public decimal? MinValue { get; set; }
        public decimal? MaxValue { get; set; }
        public decimal? MedianValue { get; set; }
    }
}
