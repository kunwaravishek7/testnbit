﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestProj.ViewModels;

namespace RepositoryServies.Servies
{
   public interface IPersonRepository
    {
        Task<IList<PersonViewModel>> GetAllPerson();
    }
}
